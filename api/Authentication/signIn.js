const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const url = "mongodb://kishore:kishore1234@ds161724.mlab.com:61724/switchon"

router.post('/',(req,res,next)=>{
    MongoClient.connect(url, (err, db) =>{
        if (err) throw err;
        const dbo = db.db("switchon");
        dbo.collection("authentication").find({ email: req.body.email }).toArray((err, result)=> {
          if (err) throw err;
          if(result.length ==1){
                bcrypt.compare(req.body.password, result[0].password, (errr, results) => {
                    if (errr) {
                        return res.json({
                        message: "Auth failed1"
                        });
                        db.close();
                    }
                    if (results) {
                        const token = jwt.sign(
                          {
                            email: result[0].email,
                            userId: result[0]._id
                          },
                          "switchon",
                          {
                              expiresIn: "1h"
                          }
                        );
                        res.status(200).json({
                          message: "Auth successful",
                          token: token
                        });
                        db.close();
                    } else {
                        res.json({
                            message:"Auth failed2!"
                        })
                        db.close();
                    }
                });
            } else {
              res.json({
                  message:"Auth failed3!"
              })
              db.close();
          }
        
        });
      }); 
});


module.exports = router;