const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const bcrypt = require('bcrypt');
const url = "mongodb://kishore:kishore1234@ds161724.mlab.com:61724/switchon"

router.post('/',(req,res,next)=>{
    let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(email) == false){
        res.json({
            message:"Invalid Email address!"
        })
    }
    MongoClient.connect(url, (err, db)=>{
        if (err) throw err;
        const dbo = db.db("switchon");
        dbo.collection("authentication").find({ email: req.body.email }).toArray((err, result)=> {
          if (err) throw err;
          if(result.length >=1){
              res.json({
                  message:"user already exist!"
              });
              db.close();
          } else {
            bcrypt.hash(req.body.password, 10 ,(err, hash)=>{
                if(err){
                    res.json({
                        error: err
                    })
                } else {
                    const user = {
                        email:req.body.email,
                        password:hash
                    }
                    dbo.collection('authentication').insertOne(user,{ useNewUrlParser: true}, (errr, result)=>{
                        if(errr) throw errr
                        res.status(201).json({
                            message:"Write Succesfull",
                            result:result
                        });
                        db.close();
                    });
                }
            });
        }
        
        });
      }); 
});
module.exports = router;