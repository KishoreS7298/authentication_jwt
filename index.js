const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');

const signUpRoute = require('./api/Authentication/signUp');
const signInRoute = require('./api/Authentication/signIn');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use('/signin',signInRoute);
app.use('/signup',signUpRoute);

const PORT = process.env.PORT || 5000;
app.listen(PORT,()=>{
    console.log("Listening on port 5000");
});